### 2.经验教训
• For production-use we strongly recommend to use the point-releases of OpenFOAM. As the development versions of OpenFOAM continuously get updated, OpenFOAM’s behaviour might change. Thus, users are advised to base their work entirely on point-releases of OpenFOAM. That way, once your simulation cases run, they will run indefinitely, or as long as you are able to install the respective version of OpenFOAM on a computer.

• Keep an eye on developments in OpenFOAM. A more recent version might provide some functionality or feature you desperately need. Even if you added this feature yourself to e.g. your custom solver or model, the developers of OpenFOAM might provide a cleaner or more powerful implementation of that feature. As it is easily possible to install several versions of OpenFOAM side by side on a computer, play around with the latest version.

• Build the source-code documentation of your local installation. It is located e.g. in $HOME/OpenFOAM/OpenFOAM-2.3.x/doc/Doxygen if you installed OpenFOAM in your home directory. This makes you independent of being online and the doxygen gives you e.g. a very well-structured overwiew of a classes methods and members.

• Study the code. Even as “the documentation is in the code” does not sound helpful at all, the code in fact tells you what is going on provided you are able to make sense of the C++ syntax. Become familiar with basic concepts of object-oriented (OO) software design.

• The more I used and tinkered with OpenFOAM, the more I am convinced that its design is really ingenious. However, it takes time and effort to come to this conclusion. It is also probably a matter of taste.

• Document your own work and stuff you tried. There is no need to create hundreds of pages, but paper or dead electrons have a longer memory as mere mortal humans. Furthermore, the fact “I have already tried X at some point in the past, and I wrote it down at Y” is more likely to be remembered than “I tried X, and that’s how it went in all detail”.

#### 2.1 哲学
OpenFOAM is largely following the general rules of the UNIX philosophy – see e.g. Eric S. Raymond [20] or http://www.catb.org/esr/writings/taoup/html/ch01s06.html – by accident, by design or by law.

1. Rule of Modularity: Write simple parts connected by clean interfaces. We see this rule in action, when we take a look at all the small pre- and post-processing    
2. Rule of Clarity: Clarity is better than cleverness.    
3. Rule of Composition: Design programs to be connected to other programs. OpenFOAM’s extensive use of text files can be interpreted as a consequence of the Rule of Composition. The structured, textual formal makes it easy to define and interpret OpenFOAM’s in- and output.    
4. Rule of Separation: Separate policy from mechanism; separate interfaces from engines.    
5. Rule of Simplicity: Design for simplicity; add complexity only where you must.    
6. Rule of Parsimony: Write a big program only when it is clear by demonstration that nothing else will do. Again, OpenFOAM is a large collection of specialized tools, rather than a big monolithic – one size fits nobody – monster.    
7. Rule of Transparency: Design for visibility to make inspection and debugging easier. Here, we quote Eric S. Raymond1
: “A software system is transparent when you can look at it and immediately understand what it is doing and how.” CFD is admittedly very complex, however, the close￾to-mathematical notation of OpenFOAM’s high-level code, can be seen as an example of OpenFOAM’s obedience to the Rule of Transparency.    
8. Rule of Robustness: Robustness is the child of transparency and simplicity.     
9. Rule of Representation: Fold knowledge into data so program logic can be stupid and robust. Although this rule was stated without object-orientation in mind, we can observe, that OpenFOAM’s data structures and classes absorb much of the complexity. Thus, the top level solver source code looks quite unspectacular.    
10. Rule of Least Surprise: In interface design, always do the least surprising thing. We see this rule in action, when we look at all the shared command line options. All tools that support time selection offer common options, such as latestTime or noZero.    
11. Rule of Silence: When a program has nothing surprising to say, it should say nothing. This rule is obeyed by most function objects, which provide the user with the choice of deactivating writing to the Terminal. This output may be useful during testing. As soon as the case is properly set up, however, it is sufficient for the function object to write its output to the corresponging file in the folder postProcessing.    
12. Rule of Repair: When you must fail, fail noisily and as soon as possible. Ever noticed the FOAM FATAL ERROR messages?    
13. Rule of Economy: Programmer time is expensive; conserve it in preference to machine time. If we allow ourselves a very broad view of this rule, we might postulate, that OpenFOAM’s mechanism to specify default values for keywords2 is one example for following this rule from a user’s perspective, i.e. it is the user’s time which is conserved.    
14. Rule of Generation: Avoid hand-hacking; write programs to write programs when you can. We can see the heavy use of 
emplates as an example of OpenFOAM following the Rule of Generation. The TurbulenceModels framework3 is an example of a modelling framework, which is coded once and applied in several different incarnations. However, this applies only in a wider sense, since this rule was stated not with C++’s templates in mind.    
15. Rule of Optimization: Prototype before polishing. Get it working before you optimize it.    
16. Rule of Diversity: Distrust all claims for “one true way”. OpenFOAM offers the user plenty of choice such as the solvers to use, the solution algorithms, and discretisation and interpolation schemes.    
17. Rule of Extensibility: Design for the future, because it will be here sooner than you think. OpenFOAM sometimes exhibits a different behaviour based on its version, or the format of the input files. See Section 36.4.1 for an example on differences in the input syntax of fixedValue boundary conditions. The important lesson in this case is to allow for evolution of the code without breaking compatibility.    

#### 2.2 Learning by using OpenFOAM
• Numerical errors can ruin your day in CFD. Not every simulation crash is the fault of some bug in OpenFOAM. The numerics of CFD is also keen to crash simulations.    
• Never deactivate the unit checking of OpenFOAM. FYI: It can be done in the global controlDict in the etc directory of your OpenFOAM installation.    
• Many classes provide optional debug information. Debug flags can be controlled via a global controlDict as well as the case’s controlDict.     
• Play around! A great part of learning is trial and error. Although many of us regard themselves as scientists or aspire to become scientists, never disregard the value of plain trail and error.    

#### 2.3 Learning by tinkering with OpenFOAM
##### 2.3.1 I learned something today.
• Have a look at the test directory in the applications folder of your installation, e.g. in $HOME/OpenFOAM/OpenFOAM-2.3.x/applications/test. There, you find examples of how to use certain data structures, which may be exactly what you need when implementing something.    
• Create your own test application, if you are about to implement something new. With a test application, you can keep the problem nearly primitive, thus, allowing yourself more mental freedom to explore and to learn. Later, you might be more likely to implement your solver / library with less bugs and errors.    
• OpenFOAM makes heavy use of C++’s language features and other smart moves in OO software design. Thus, make sure you understand the basics of the following concepts / language features before you try to study / modify the code of OpenFOAM. Your life gets easier if you do. inheritance virtually everything of OpenFOAM is described and implemented using the concept of classes. Classes can be derived from other classes to implement an is a relationship, i.e. every cat is an animal
but not vice versa. Note: C++ support multiple inheritance, i.e. a class can be derived from a number of classes, not just
one. Other programming languages are (slightly) different in this aspect, e.g. Java allows you to derive only from one class, however, you can implement interfaces. poly-morphism is a wider concept, however it applies also to inheritance and classes.
templates allow the user to write code for as-of-yet unspecified data types. Container classes are the prime example for the use of templates (or generics as this concept is called in Java). Examples of the excellent use of the aforementioned concepts is the turbulence modelling framework discussed in Section 32.1.2, or the Lagrangian modelling framework discussed in Section 40.2.    
##### 2.3.2 Trouble with the code?    
it does not compile    
• Due to the heavy use of templates the syntax and the compiler error messages are quite lengthy and often hard to read. However, the compiler error message might contain exactly the information you need to track down the error, e.g. a data-type mismatch. Familiarize yourself with C++’s syntax if you haven’t already.    
If you are baffled by the very lengthy error messages, take special care to the top and the bottom of the error message, as it is there where you might find the most useful clues. it does not run    
• Spurious crashes (e.g. caused by floating point errors) may be an indication of class members being un-initialized.    
• No offence, but it’s most probably your fault.    